/*
 * SPDX-FileCopyrightText: 2018 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.7
import QtQuick.Controls 2.0 as Controls2
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.0 as Kirigami

/*
 * Component that displays the days of a month as a 6x7 table
 *
 * Optionally, it may display:
 * - a header on top of the table showing the current date
 * - inside each day cell, a small indicator in case that tasks
 *   exist for this day
 */
Item {
    id: root

    property int days: 7
    property int weeks: 6
    property date currentDate: new Date()
    property int dayRectWidth: Kirigami.Units.gridUnit * 2.5
    property date selectedDate: new Date()
    property int selectedDayTodosCount: 0
    property int selectedDayEventsCount: 0
    property string displayedMonthName
    property int displayedYear
    property var reloadSelectedDate: function() {}
    property var applicationLocale: Qt.locale()
    property bool loadAsync: false
    property double borderWidth: 1.0
    property double borderOpacity: 0.8


    /**
     * A model that provides:
     *
     * 1. dayNumber
     * 2. monthNumber
     * 3. yearNumber
     */
    property var daysModel

    property bool showHeader: false
    property bool showMonthName: true
    property bool showYear: true

    onSelectedDateChanged: reloadSelectedDate()

    Loader {
        id: monthViewLoader

        anchors.centerIn: parent

        visible: status == Loader.Ready
        sourceComponent: viewLayoutComponent
        asynchronous: root.loadAsync
    }

    Controls2.BusyIndicator {
        anchors.centerIn: parent

        running: !monthViewLoader.visible
        implicitWidth: Kirigami.Units.iconSizes.enormous
        implicitHeight: width
    }

    Component {
        id: viewLayoutComponent

        ColumnLayout {
            id: viewLayout

            anchors.centerIn: parent

            spacing: Kirigami.Units.gridUnit / 4

            /**
            * Optional header on top of the table
            * that displays the current date and
            * the amount of the day's tasks
            */
            CalendarHeader {
                id: calendarHeader

                Layout.bottomMargin: Kirigami.Units.gridUnit / 2
                applicationLocale: root.applicationLocale
                headerDate: root.selectedDate
                headerTodosCount: root.selectedDayTodosCount
                headerEventsCount: root.selectedDayEventsCount
                visible: root.showHeader
            }


            RowLayout {

                Controls2.Label {
                    visible: showMonthName
                    font.pointSize: Kirigami.Units.fontMetrics.font.pointSize * 1.5
                    text: displayedMonthName
                }

                Controls2.Label {
                    visible: showYear
                    font.pointSize: Kirigami.Units.fontMetrics.font.pointSize * 1.5
                    text: displayedYear
                }
            }



            Item {
                width: canvas.width
                height: canvas.height

                //Code adapted from plasma-frameworks' DaysCalendar
                Canvas {
                    id: canvas

                    width: (root.dayRectWidth + root.borderWidth) * root.days + root.borderWidth
                    height: (root.dayRectWidth + root.borderWidth) * (root.weeks + 1)  + root.borderWidth

                    opacity: root.borderOpacity
                    antialiasing: false
                    clip: false
                    onPaint: {
                        var ctx = getContext("2d");

                        ctx.reset();
                        ctx.save();
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.strokeStyle = Kirigami.Theme.textColor;
                        ctx.lineWidth = 1.0;
                        ctx.globalAlpha = 1.0;

                        ctx.beginPath();

                        // horizontal lines
                        for (var i = 0; i < root.weeks + 2; i++) {
                            var lineY = (root.dayRectWidth + root.borderWidth) * i;
                            ctx.moveTo(0, lineY);
                            ctx.lineTo(width, lineY);
                        }

                        // vertical lines
                        for (var i = 0; i < root.days + 1; i++) {
                            var lineX = (root.dayRectWidth + root.borderWidth) * i;

                            // Draw the outer vertical lines in full height so that it closes
                            // the outer rectangle
                            if (i === 0 || i === root.days) {
                                ctx.moveTo(lineX, 0);
                            } else {
                                ctx.moveTo(lineX, root.dayRectWidth + root.borderWidth);
                            }
                            ctx.lineTo(lineX, height);
                        }

                        ctx.closePath();
                        ctx.stroke();
                        ctx.restore();
                    }
                }

                Grid {
                    id: calendarGrid

                    anchors {
                        top: canvas.top
                        topMargin: root.borderWidth
                        left: canvas.left
                        leftMargin: root.borderWidth
                    }

                    columns: root.days
                    rows: root.weeks + 1

                    spacing: root.borderWidth
                    property bool containsEventItems: false // FIXME
                    property bool containsTodoItems: false // FIXME

                    /**
                     * Styled week day names of the days' calendar grid
                     * E.g.
                     * Mon Tue Wed ...
                     */
                    Repeater {
                        id: days

                        model: calendarGrid.columns
                        delegate: Controls2.Label {
                            width: root.dayRectWidth
                            height: root.dayRectWidth
                            text: root.applicationLocale.dayName(((model.index + root.applicationLocale.firstDayOfWeek) % root.days), Locale.ShortFormat)
                            font.pixelSize: root.dayRectWidth / 3
                            opacity: 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight
                            fontSizeMode: Text.HorizontalFit
                        }
                    }

                    // Days of the month
                    Repeater {
                        id: repeater

                        model: root.daysModel
                        delegate:   DayDelegate {
                            id: delegate
                            width: root.dayRectWidth
                            height: root.dayRectWidth

                            currentDate: root.currentDate
                            delegateWidth: root.dayRectWidth
                            selectedDate: root.selectedDate

                            onDayClicked: root.selectedDate = new Date(model.yearNumber, model.monthNumber -1, model.dayNumber, root.selectedDate.getHours(), root.selectedDate.getMinutes(), 0)
                        }
                    }
                }
            }
        }
    }

    //Redraw canvas if current theme color changes
    Connections {
        target: theme
        function onTextColorChanged() {
            canvas.requestPaint();
        }
    }
}
